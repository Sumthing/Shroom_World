extends RigidBody2D

#This is the shotgun hitbox if these things enter then it expodes
func _on_Area2D_body_entered(body):
	if "TileMap" in body.name:
		$Explosion_Radius/CollisionShape2D.set_deferred("disabled", false)
		$Area2D/CollisionShape2D.set_deferred("disabled", true)
		$Sprite.hide()
		$Particles2D.emitting = true
		#Timer to delete the bullet
		$Timer.start()
	
	if "Enemy" in body.name:
		$Explosion_Radius/CollisionShape2D.set_deferred("disabled", false)
		$Area2D/CollisionShape2D.set_deferred("disabled", true)
		$Sprite.hide()
		$Particles2D.emitting = true
		#Timer to delete the bullet
		$Timer.start()
	
	pass # Replace with function body.s


func _on_Timer_timeout():
	queue_free()
	pass # Replace with function body.
