extends Enemy

var Gas = preload("res://Scenes/Misc Scenes/Bloomer_gas.tscn")

func _physics_process(delta):
	
	if hp <= 0:
		var gas = Gas.instance()
		gas.position = $".".get_global_position()
		get_tree().get_root().add_child(gas)
	
	pass
