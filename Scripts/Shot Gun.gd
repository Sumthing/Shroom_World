extends Sprite

#Weapon variables
var has_gun = false
var bullet = preload("res://Scenes/Misc Scenes/ShotGun Bullet.tscn")
var bullet_speed = 1000

export var cur_ammo = 21

func _physics_process(delta):
	
	$".".look_at(get_global_mouse_position())
	
	shoot()
	
	pass


#shoot Function Logic
func shoot():
	if Input.is_action_just_pressed("Shoot") and has_gun and cur_ammo > 0:
		
		#plays sound
		$AudioStreamPlayer2D.play()
		
		var Bullet = bullet.instance()
		
		cur_ammo -= 3
		#the position in which the bullet spawns will be the mouse position
		Bullet.position = $Position2D.get_global_position()
		
		#Apply an impulse on bullet to make it move with the variable bulllet speed
		Bullet.apply_impulse(Vector2(), Vector2(bullet_speed, 0).rotated(rotation))
	
		get_tree().get_root().add_child(Bullet)
	
	pass
