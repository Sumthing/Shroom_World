extends Area2D

export var id = 1
var level

func _physics_process(delta):
	
	if id == 1:
		level = "res://Levels/Fishing Village.tscn"
	
	pass

func _on_LevelExit_body_entered(body):
	
	if "Player" in body.name:
		$Transitions/canvas/pixelation.show()
		$Transitions/trans_anim.play("Pixelation")
		yield(get_tree().create_timer(0.4), "timeout")
		get_tree().change_scene(level)
	
	pass # Replace with function body.
