extends Sprite

#Weapon variables
var has_gun = false
var bullet = preload("res://Scenes/Misc Scenes/Bullet.tscn")
var bullet_speed = 1000

export var cur_ammo = 24


func _physics_process(delta):
	
	$".".look_at(get_global_mouse_position())
	
	shoot()
	
	pass


#shoot Function Logic
func shoot():
	if Input.is_action_just_pressed("Shoot") and has_gun and cur_ammo > 0:
		var Bullet = bullet.instance()
		cur_ammo -= 1
		#plays sound
		$AudioStreamPlayer2D.play()
		#the position in which the bullet spawns will be the mouse position
		Bullet.position = $Position2D.get_global_position()
		Bullet.rotation_degrees = rotation_degrees
		#Apply an impulse on bullet to make it move with the variable bulllet speed
		Bullet.apply_impulse(Vector2(), Vector2(bullet_speed, 0).rotated(rotation))
		get_tree().get_root().add_child(Bullet)
	
	pass
