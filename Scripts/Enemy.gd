extends KinematicBody2D

class_name Enemy

#movement variables
var motion = Vector2()
export var speed = 150

#Stats
var hp = 5

#targeting variables
var help
var spotted = false
var target
onready var Player = get_parent().get_node("Player")

#CONDITIONAL VARIABLES
var stunned = false

func _ready():
	Global.connect("spot", self, "alert")
	
	pass

func _physics_process(delta):
	
	if stunned == false:
		chase(delta)
	
	flip()
	
	move_and_slide(motion)
	pass

#function to make enemy go towards player to attack him
func chase(delta):
	
	if target and spotted:
		var target_dir = (target.global_position - global_position).normalized()
		var cur_dir = Vector2(1, 0)
		$AnimationPlayer.play("walk")
		motion = motion.move_toward(target_dir * speed, 300 * delta)
		Global.emit_signal("spot")
	
	pass

func stun():
	#to stun enemy after he/she gets hit
	stunned = true
	speed = 0
	yield(get_tree().create_timer(0.3), "timeout")
	speed = 150
	stunned = false

#function for killing the enemy
func hit():
	hp = hp - 1
	target = Player
	spotted = true
	if hp <= 0:
		queue_free()
	$AnimationPlayer.play("Hurt")
	stun()
	pass


func flip():
	
	if Player.position.x < position.x:
		$Sprite.scale.x = -2
	
	if Player.position.x > position.x:
		$Sprite.scale.x = 2

func alert():
	
	if spotted == false and help:
		spotted = true
	
	pass

#singnals
func _on_Sight_body_entered(body):
	
	if "Player" in body.name:
		spotted = true
	
	
	pass # Replace with function body.

func _on_HitBox_body_entered(body):
	
	if body.is_in_group("Player"):
		hit()
		body.queue_free()
	
	pass # Replace with function body.

func _on_HitBox_area_entered(area):
	
	if "Explosion_Radius" in area.name:
		hit()
	
	pass # Replace with function body.


func _on_OuterSight_body_entered(body):
	
	if "Player" in body.name:
		target = body
		pass
	
	if body.is_in_group("Enemy"):
		help = body
	
	pass # Replace with function body.


