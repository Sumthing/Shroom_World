extends Sprite

#laser vars
const max_length = 1

onready var beam = $Beam
onready var end = $Position2D
onready var Raycast = $RayCast2D

#Weapon variables
var has_gun = false
var bullet = preload("res://Scenes/Misc Scenes/Bullet.tscn")
var bullet_speed = 1000

export var cur_ammo = 32
var clip_ammo = 5

func _physics_process(delta):
	
	#laser code
	var mouse_postion = get_local_mouse_position()
	#var max_cast_to = mouse_postion.normalized() * 10
	#Raycast.rotation_degrees = mouse_postion
	Raycast.scale.x += max_length
	if Raycast.is_colliding():
		Raycast.scale.x = 1
		end.global_position = Raycast.get_collision_point()
	else:
		end.global_position = Raycast.cast_to
	beam.rotation = Raycast.cast_to.angle()
	beam.region_rect.end
	
	#non lazer stuff
	if clip_ammo <= 0:
		has_gun = false
		yield(get_tree().create_timer(0.4), "timeout")
		clip_ammo = 5
	
	$".".look_at(get_global_mouse_position())
	
	shoot()
	
	pass


#shoot Function Logic
func shoot():
	if Input.is_action_just_pressed("Shoot") and has_gun and cur_ammo > 0:
		has_gun = false
		
		#plays sound
		$AudioStreamPlayer2D.play()
		
		var Bullet = bullet.instance()
		cur_ammo -= 1
		clip_ammo -= 1 
		#the position in which the bullet spawns will be the mouse position
		Bullet.position = $Position2D.get_global_position()
		Bullet.rotation_degrees = rotation_degrees
		#Apply an impulse on bullet to make it move with the variable bulllet speed
		Bullet.apply_impulse(Vector2(), Vector2(bullet_speed, 0).rotated(rotation))
		get_tree().get_root().add_child(Bullet)
		yield(get_tree().create_timer(0.1),"timeout")
		has_gun = true
	
	pass
