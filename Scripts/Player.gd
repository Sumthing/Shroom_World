extends KinematicBody2D

var arya

onready var ammo_label = $ParallaxBackground/Label

#player statistics
var hp = 3

var pickable_P = false
var pickable_M = false
var pickable_S = false

var can_getHit = true

var poison = false
#movement Variables
var motion = Vector2()
var speed = 300

#Weapon variables

var Cur_Weapon = 1

var Weapon


#physics process 
func _physics_process(delta):
	
	move(delta)
	
	Weapon_selected()
	
	flip()
	
	poison()
	
	pass


#movement function
func move(delta):
	
	
	if Input.is_action_pressed("ui_right"):
		motion.x = speed
		$Node2D/AnimatedSprite.play("walk")
	
	elif Input.is_action_pressed("ui_left"):
		motion.x = -speed
		$Node2D/AnimatedSprite.play("walk")
	
	else:
		motion.x = 0
	
	if Input.is_action_pressed("ui_down"):
		motion.y = speed
		$Node2D/AnimatedSprite.play("walk")
	
	elif Input.is_action_pressed("ui_up"):
		motion.y = -speed
		$Node2D/AnimatedSprite.play("walk")
	
	else:
		motion.y = 0
	
	if motion.y == 0 and motion.x == 0 and can_getHit == true:
		$Node2D/AnimatedSprite.play("idle")
	
	
	move_and_slide(motion)
	
	pass


#funtion to flip the sprite depending on mouse position
func flip():
	
	if get_global_mouse_position() < global_position:
		$Node2D/AnimatedSprite.scale.x = -2
		$"Node2D/Weapons/Machine Gun".scale.y = -2
		$Node2D/Weapons/pistol.scale.y = -2
		$"Node2D/Weapons/Shot Gun".scale.y = -2
	
	else:
		$Node2D/AnimatedSprite.scale.x = 2
		$"Node2D/Weapons/Machine Gun".scale.y = 2
		$Node2D/Weapons/pistol.scale.y = 2
		$"Node2D/Weapons/Shot Gun".scale.y = 2


#Logic for When hit
func hit():
	if can_getHit:
		hp = hp - 1
		HP()
		can_getHit = false
		$Node2D/AnimatedSprite.play("Hurt")
		$"Camera2D/camera animation player".play("Screen Shake")
		yield(get_tree().create_timer(0.6), "timeout")
		can_getHit = true
	
	if hp <= 0:
		get_tree().reload_current_scene()
	
	pass

#to damage the player due to bloomer gas
func poison():
	if poison:
		hit()
		yield(get_tree().create_timer(1), "timeout")
		if poison:
			hit()
		poison = false



#To check the HP to update HP bar
func HP():
	
	if hp == 2:
		$AnimationPlayer.play("Hit")
	if hp == 1:
		$AnimationPlayer.play("Hit 2")

#function for checking which weapon is in use
func Weapon_selected():
	
	if Cur_Weapon == 1:
		$Node2D/Weapons/pistol.visible = true
		$Node2D/Weapons/pistol.has_gun = true
		Weapon = preload("res://Scenes/Misc Scenes/pistol.tscn")
		#so that all other weapons begon
		$"Node2D/Weapons/Machine Gun".visible = false
		$"Node2D/Weapons/Machine Gun".has_gun = false
		$"Node2D/Weapons/Shot Gun".visible = false
		$"Node2D/Weapons/Shot Gun".has_gun = false
	
	elif Cur_Weapon == 2:
		$"Node2D/Weapons/Machine Gun".visible = true
		$"Node2D/Weapons/Machine Gun".has_gun = true
		Weapon = preload("res://Scenes/Misc Scenes/Machine Gun.tscn")
		#so that all other weapons begon
		$Node2D/Weapons/pistol.visible = false
		$Node2D/Weapons/pistol.has_gun = false
		$"Node2D/Weapons/Shot Gun".visible = false
		$"Node2D/Weapons/Shot Gun".has_gun = false
	
	if Cur_Weapon == 3:
		$"Node2D/Weapons/Shot Gun".visible = true
		$"Node2D/Weapons/Shot Gun".has_gun = true
		Weapon = preload("res://Scenes/Misc Scenes/Shot Gun.tscn")
		#so that all other weapons begon
		$Node2D/Weapons/pistol.visible = false
		$Node2D/Weapons/pistol.has_gun = false
		$"Node2D/Weapons/Machine Gun".visible = false
		$"Node2D/Weapons/Machine Gun".has_gun = false
	
	pistol_equip()
	M_equip()
	S_equip()
	
	pickup()
	ammmo_l()
	pass

#for ammo
func ammmo_l():
	
	ammo_label.set_text("P: %d" % [$Node2D/Weapons/pistol.cur_ammo, ])
	

#for equiping weapons

#for equipping the pistol
func pistol_equip():
	
	
	if Input.is_action_just_pressed("interact") and pickable_P:
		$Node2D/Weapons/pistol.visible = true
		Cur_Weapon = 1
		arya.queue_free()
		yield(get_tree().create_timer(0.001), "timeout")
		var Pistol = Weapon.instance()
		Pistol.position = $"Node2D/Weapons".get_global_position()
		Pistol.rotation_degrees = rotation_degrees
		get_tree().get_root().add_child(Pistol)
	
	pass


#for equipping the Lazer gun
func M_equip():
	
	if Input.is_action_just_pressed("interact") and pickable_M:
		Cur_Weapon = 2
		arya.queue_free()
		yield(get_tree().create_timer(0.001), "timeout")
		$"Node2D/Weapons/Machine Gun".visible = true
		var M_gun = Weapon.instance()
		M_gun.position = $"Node2D/Weapons".get_global_position()
		M_gun.rotation_degrees = rotation_degrees
		get_tree().get_root().add_child(M_gun)

#for equipping the shotgun
func S_equip():
	if Input.is_action_just_pressed("interact") and pickable_S:
		Cur_Weapon = 3
		arya.queue_free()
		yield(get_tree().create_timer(0.001), "timeout")
		$"Node2D/Weapons/Shot Gun".visible = true
		var S_gun = Weapon.instance()
		S_gun.position = $"Node2D/Weapons".get_global_position()
		S_gun.rotation_degrees = rotation_degrees
		get_tree().get_root().add_child(S_gun)
	
	pass

#to make the pickup sprite visibles
func pickup():
	
	if pickable_P or pickable_M or pickable_S:
		$Node2D/Pickup.visible = true
	
	else:
		$Node2D/Pickup.visible = false
	
	pass


#signals
func _on_hitbox_body_entered(body):
	
	#result for when player touches an enemy
	if body.is_in_group("Enemy"):
		hit()
	
	#result for when player gets hit by a bullet
	if "Enemy_Bullet" in body.name:
		body.queue_free()
	
	pass # Replace with function body.


func _on_hitbox_area_entered(area):
	
	if area.is_in_group("M_gun"):
		pickable_M = true
		arya = area
	
	
	if area.is_in_group("pistol"):
		pickable_P = true
		arya = area
	
	
	if area.is_in_group("S_gun"):
		pickable_S = true
		arya = area
	
	
	if area.is_in_group("Ammo"):
		$"Node2D/Weapons/Machine Gun".cur_ammo += 24
		$Node2D/Weapons/pistol.cur_ammo += 12
		$"Node2D/Weapons/Shot Gun".cur_ammo += 15
		area.queue_free()
	
	if area.is_in_group("HP_kit"):
		hp = 3
		$AnimationPlayer.play("Full Hp")
		area.queue_free()
	
	if area.is_in_group("Gas"):
		poison = true
	
	pass # Replace with function body.


func _on_hitbox_area_exited(area):
	
	if area.is_in_group("pistol"):
		pickable_P = false
	
	if area.is_in_group("M_gun"):
		pickable_M = false
	
	if area.is_in_group("S_gun"):
		pickable_S = false
	
	if area.is_in_group("Gas"):
		poison = false
	
	pass # Replace with function body.

